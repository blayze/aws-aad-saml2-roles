# Purpose
- The purpose of this repository is to complement this tutorial: https://docs.microsoft.com/en-us/azure/active-directory/active-directory-saas-amazon-web-service-tutorial

# Assumptions
- Import the metadata xml first, and name the IdP 'AAD'.
- The roles can be modified, you probably don't need the same as I did. This is just a template for creating roles which verify trust with the IdP.

# General Overview
- Replaces repetition of steps 14 through 20.
- Replaces the need for the root credentials in step 21.
- Provides keys as output which AAD uses in Provisioning.
- Non-Admins are denied CloudFormation access so they can't see keys for the user with full IAM access.